const Function = require('./functions.js');

const pal = [
    'No lemon, no melon',
    'Top Spot',
    'Spontan Uhuy',
];

const n = [2, 5, 10];

console.log('------------------ Here is My Test -------------------');

console.log('1. Write a Python/NodeJS function to check if a string is a palindrome');
pal.forEach((palindrome) => {
    console.log(`palindrom string = ${palindrome}\n`, Function.isPalindrome(palindrome), '\n');
});

console.log('2. Write a Python/NodeJS function that will return a fibonacci number based on iteration index that will be given as function argument ');
n.forEach((numberOfIndex) => {
    console.log(`n = ${numberOfIndex}\n`, Function.fibonnaci(numberOfIndex), '\n');
});

console.log('3. Write a Python/NodeJS with DRY principle')
console.log(Function.isDryPrinciple());