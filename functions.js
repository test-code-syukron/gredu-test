class Function {

    static fibonnaci(n) {
        // check the type data is a number or not.
        const isNumber  = typeof n === 'number';
        if (!isNumber) {
            return 'n should be a number!';
        } 
        
        // initialize fibonnaci number
        const result = [0, 1];

        // loop and count the fibonnaci
        for (var i = 2; i <= n; i++) {
            const a = result[i - 1];
            const b = result[i - 2];
            result.push(a + b);
        }

        return result;
    }

    static isPalindrome(string) {
        // check the type data is a number or not.
        const isString  = typeof string === 'string';
        if (!isString) {
            return 'value should be a string!'
        } 

        // Change to lower string
        const loweredString = string.toLowerCase();
        // remove the space
        const fixedString = loweredString.replace(/ /g, '');
        // Length of wordss
        const len = fixedString.length;  

        // divide the words into 2 half  
        for (let i = 0; i < len / 2; i++) {  

            // check first and last characters are same  
            if (fixedString[i] !== fixedString[len - 1 - i]) {  
                return( 'No, It isn\'t a palindrome');  
            }  
        }

        return 'Yes, it is a palindrome';  
    }

    static isDryPrinciple() {
        return 'by creating all of this function i already use the DRY principle.'
    }
}

module.exports = Function;